/******************************************************************************
 * Copyright 2023 Qsaker(wuuhaii@outlook.com). All rights reserved.
 *
 * The file is encoded using "utf8 with bom", it is a part
 * of QtSwissArmyKnife project.
 *
 * QtSwissArmyKnife is licensed according to the terms in
 * the file LICENCE in the root of the source code directory.
 *****************************************************************************/
#ifndef SAKTABLEVIEWTOOLUI_HH
#define SAKTABLEVIEWTOOLUI_HH

#include "SAKBaseToolUi.h"
#include "SAKTableModel.h"

class SAKTableViewToolUi : public SAKBaseToolUi
{
public:
    SAKTableViewToolUi(QWidget *parent = nullptr);
};

#endif // SAKTABLEVIEWTOOLUI_HH
