INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/SAKAnalyzerToolUi.hh \
    $$PWD/SAKBaseToolUi.hh \
    $$PWD/SAKBleCentralToolUi.hh \
    $$PWD/SAKCommunicationToolUi.hh \
    $$PWD/SAKEmitterToolUi.hh \
    $$PWD/SAKEmitterToolUiEditor.hh \
    $$PWD/SAKMaskerToolUi.hh \
    $$PWD/SAKPrestorerToolUi.hh \
    $$PWD/SAKPrestorerToolUiEditor.hh \
    $$PWD/SAKResponserToolUi.hh \
    $$PWD/SAKResponserToolUiEditor.hh \
    $$PWD/SAKSerialPortToolUi.hh \
    $$PWD/SAKSerialPortTransmitterToolUi.hh \
    $$PWD/SAKSerialPortTransmitterToolUiEditor.hh \
    $$PWD/SAKSocketClientToolUi.hh \
    $$PWD/SAKSocketClientTransmitterToolUi.hh \
    $$PWD/SAKSocketClientTransmitterToolUiEditor.hh \
    $$PWD/SAKSocketServerToolUi.hh \
    $$PWD/SAKStatisticianToolUi.hh \
    $$PWD/SAKStorerToolUi.hh \
    $$PWD/SAKTableModelToolUi.hh \
    $$PWD/SAKTcpTransmitterToolUi.hh \
    $$PWD/SAKTransmitterToolUi.hh \
    $$PWD/SAKUdpTransmitterToolUi.hh \
    $$PWD/SAKVelometerToolUi.hh \
    $$PWD/SAKWebSocketTransmitterToolUi.hh

SOURCES += \
    $$PWD/SAKAnalyzerToolUi.cc \
    $$PWD/SAKBaseToolUi.cc \
    $$PWD/SAKBleCentralToolUi.cc \
    $$PWD/SAKCommunicationToolUi.cc \
    $$PWD/SAKEmitterToolUi.cc \
    $$PWD/SAKEmitterToolUiEditor.cc \
    $$PWD/SAKMaskerToolUi.cc \
    $$PWD/SAKPrestorerToolUi.cc \
    $$PWD/SAKPrestorerToolUiEditor.cc \
    $$PWD/SAKResponserToolUi.cc \
    $$PWD/SAKResponserToolUiEditor.cc \
    $$PWD/SAKSerialPortToolUi.cc \
    $$PWD/SAKSerialPortTransmitterToolUi.cc \
    $$PWD/SAKSerialPortTransmitterToolUiEditor.cc \
    $$PWD/SAKSocketClientToolUi.cc \
    $$PWD/SAKSocketClientTransmitterToolUi.cc \
    $$PWD/SAKSocketClientTransmitterToolUiEditor.cc \
    $$PWD/SAKSocketServerToolUi.cc \
    $$PWD/SAKStatisticianToolUi.cc \
    $$PWD/SAKStorerToolUi.cc \
    $$PWD/SAKTableModelToolUi.cc \
    $$PWD/SAKTcpTransmitterToolUi.cc \
    $$PWD/SAKTransmitterToolUi.cc \
    $$PWD/SAKUdpTransmitterToolUi.cc \
    $$PWD/SAKVelometerToolUi.cc \
    $$PWD/SAKWebSocketTransmitterToolUi.cc

FORMS += \
    $$PWD/SAKAnalyzerToolUi.ui \
    $$PWD/SAKBleCentralToolUi.ui \
    $$PWD/SAKEmitterToolUiEditor.ui \
    $$PWD/SAKMaskerToolUi.ui \
    $$PWD/SAKPrestorerToolUiEditor.ui \
    $$PWD/SAKResponserToolUiEditor.ui \
    $$PWD/SAKSerialPortToolUi.ui \
    $$PWD/SAKSerialPortTransmitterToolUiEditor.ui \
    $$PWD/SAKSocketClientToolUi.ui \
    $$PWD/SAKSocketClientTransmitterToolUiEditor.ui \
    $$PWD/SAKSocketServerToolUi.ui \
    $$PWD/SAKStatisticianToolUi.ui \
    $$PWD/SAKStorerToolUi.ui \
    $$PWD/SAKTableModelToolUi.ui \
    $$PWD/SAKVelometerToolUi.ui \
