INCLUDEPATH += $$PWD

FORMS += \
    $$PWD/SAKToolBoxUi.ui \
    $$PWD/SAKToolBoxUiCommunicationMenu.ui \
    $$PWD/SAKToolBoxUiInputMenu.ui \
    $$PWD/SAKToolBoxUiOutputMenu.ui

HEADERS += \
    $$PWD/SAKToolBoxUi.hh \
    $$PWD/SAKToolBoxUiCommunicationMenu.hh \
    $$PWD/SAKToolBoxUiInputMenu.hh \
    $$PWD/SAKToolBoxUiOutputMenu.hh

SOURCES += \
    $$PWD/SAKToolBoxUi.cc \
    $$PWD/SAKToolBoxUiCommunicationMenu.cc \
    $$PWD/SAKToolBoxUiInputMenu.cc \
    $$PWD/SAKToolBoxUiOutputMenu.cc
