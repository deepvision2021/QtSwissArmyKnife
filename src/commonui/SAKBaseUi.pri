INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/SAKComboBox.hh \
    $$PWD/SAKLineEdit.hh

SOURCES += \
    $$PWD/SAKComboBox.cc \
    $$PWD/SAKLineEdit.cc
